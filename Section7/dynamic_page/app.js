const path = require('path')
const express = require('express')
const hbs = require('hbs')

const app = express()

app.set('view engine', 'hbs')
// custom view
const viewsPath = path.join(__dirname, 'src')
app.set('views', viewsPath)
// template path
const partialsPath = path.join(__dirname, 'src/template')
hbs.registerPartials(partialsPath)

app.get('/', (req, res) => {
    res.render('index', {
        title: 'My Title',
        name: 'Me'
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'Muhammad Rizki Halomoan',
        errorMessage: 'Page not found.'
    })
})


app.listen(3001, () => {
    console.log('Server is up on port 3001.')
}) 