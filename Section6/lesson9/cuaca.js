const request = require('request')

const cuaca =  (id, callback) => {
    const url = 'https://ibnux.github.io/BMKG-importer/cuaca/'+ id +'.json'

    request({ url: url, json: true }, (error, response) => {
        if(error){
            callback('Unable to connect to weather services', undefined)
        }else{
            callback('undefined', {
                jam: 'tengah malam',
                cuaca: response.body[0].cuaca,
                temp: response.body[0].tempC,
            })
        }
    })
}

module.exports = cuaca