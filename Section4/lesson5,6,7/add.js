const book = require('./book')
const yargs = require('yargs')

yargs.command({
    command: 'add',
    describe: 'Add a new book',
    builder: {
        title: {
            describe: 'Book Title',
            demandOption: true,
            type: 'string'
        },
        author: {
            describe: 'Book Author',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function (argv) {
        const newBook = {
            title: argv.title,
            author: argv.author
        }
        book.push(newBook)
        console.log(book)
    }
})

console.log(yargs.argv)

// run node add.js add --title="Buy" --author="Moan"