const book = require('./book')
const yargs = require('yargs')
yargs.command({
    command: 'delete',
    describe: 'Delete book',
    builder: {
        title: {
            describe: 'Book Title',
            demandOption: true,
            type: 'string'
        },
    },
    handler: function (argv) {
        const index = book.findIndex((item) => item.title === argv.title);
        console.log("book : " + index)
        if (index !== -1) {
            console.log("awal")
            console.log(book)
            book.splice(index, 1)
            console.log("setelah di delete")
            console.log(book)
        }else{
            console.log("book not found")
        }
        
    }
})

console.log(yargs.argv)

// run node delete.js delete --title="Title Deleted"