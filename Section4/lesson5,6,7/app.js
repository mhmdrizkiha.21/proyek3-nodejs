const book = require('./book')

// Covert JavaScript object into JSON string
const bookJSON = JSON.stringify(book[0])
// Covert JSON string into object
const bookObject = JSON.parse(bookJSON)
console.log(bookObject.title) // Print: Ego is the Enemy